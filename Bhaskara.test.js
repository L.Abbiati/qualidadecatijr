const Bhaskara = require('./Bhaskara')

describe('testando formula de bhaskara', () => {
    it('testando a = 1, b = -3, c = 2',() => {
        expect(Bhaskara(1, -3, 2)).toBe("Raiz 1: 2, Raiz 2: 1")
    })

    it('testando a = 1, b = 4, c = 5',() => {
        expect(Bhaskara(1, 4, 5)).toBe("Não há raízes reais")
    })

    it('testando a = 2, b = -8, c = 8',() => {
        expect(Bhaskara(2, -8, 8)).toBe(2);
    })
    
    /*
    it('testando parametros invalidos',() => {
        expect(Bhaskara(a, -8, 8)).toBe('Parâmetros Inválidos');
    })

    it('testando parametros invalidos',() => {
        expect(Bhaskara(1, b, 8)).toBe('Parâmetros Inválidos');
    })

    it('testando parametros invalidos',() => {
        expect(Bhaskara(2, -8, c)).toBe('Parâmetros Inválidos');
    })
    */
})