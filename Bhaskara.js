const Bhaskara = (a, b, c) => {

    if(isNaN(a) || isNaN(b) || isNaN(c)){
        return 'Parâmetros Inválidos'
    }

    const delta = b * b - 4 * a * c;
  
    if (delta < 0) {
      return "Não há raízes reais";
    } 
    else if (delta === 0) {
      const raiz = -b / (2 * a);
      return raiz;
    } 
    else {
      const raiz1 = (-b + Math.sqrt(delta)) / (2 * a);
      const raiz2 = (-b - Math.sqrt(delta)) / (2 * a);
      return `Raiz 1: ${raiz1}, Raiz 2: ${raiz2}`;
    }
  }

module.exports = Bhaskara;